export let renderGlassList = (list) => {
  var contentShow = "";
  list.forEach(function (item) {
    var content = `
        <img onclick="choseGlass('${item.id}')" class="col-4" src="${item.src}" alt="" />
         `;
    contentShow += content;
  });
  document.getElementById("vglassesList").innerHTML = contentShow;
};
export let findGlasses = (id, arr) => {
  let index = arr.findIndex(function (e) {
    return e.id == id;
  });
  return index;
};

export let show = (glass) => {
  document.getElementById("imgGlass").classList.remove("d-none");
  document.getElementById("glassesInfo").style.display = "block";
  document.getElementById("glassesInfo").innerHTML = `
  <h4>${glass.name} - ${glass.brand} ${glass.color}</h4>
  <h5>${glass.price}</h5>
  <p>${glass.description}</p>
  `;
  document.getElementById("imgGlass").src = glass.virtualImg;
};
